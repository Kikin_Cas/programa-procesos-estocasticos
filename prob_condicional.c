#include <stdio.h>
#include <stdlib.h>



int main(){

	int a,b,c,d,n,i,j,np;
	printf("Numero de estados: ");
	scanf("%d",&n);
	fflush(stdin);
	float v[n],aux[n];
	for (a=0;a<n;a++)
	{
		v[a]=0;
	}
		for (a=0;a<n;a++)
	{
		aux[a]=0;
	}

	// IMPRIMIR EL VALOR DE N Y EL VECTOR V
	printf("\n\n\n IMPRIMIR N Y EL VECTOR V \n\n");
	printf("n: %d\n\n",n);
	for(a=0;a<n;a++)
		printf("%f\t", v[a]); 
	

	printf("\n\nMatriz de transicion:\n");
	float m[n][n];
	for(a=0;a<n;a++)
	{
		for(b=0;b<n;b++)
		{
			printf("Elemento (%d,%d): ",a,b);
			scanf("%f",&m[a][b]);
			fflush(stdin);
		}
	}

	//IMPRIMIR LA MATRIZ M
	printf("\n\n\n IMPRIMIR LA MATRIZ N \n\n");
	for(a=0;a<n;a++)
	{
		for(b=0;b<n;b++)
			printf("%f ",m[a][b]);
		printf("\n");
	}
		


	printf("Estado inicial: ");
	scanf("%d",&i);
	fflush(stdin);
	v[i]=1;

	//IMPRIMIR ESTADO INICIAL I Y VECTOR V
	printf("i: %d\n",i );
	for(a=0;a<n;a++)
		printf("%f\t",v[a]);

	printf("\n\nEstado final: ");
	scanf("%d",&j);
	fflush(stdin);

	//IMPRIMIR ESTADO FINAL
	printf("j: %d\n",j );

	printf("\n\nNumero de periodos: ");
	scanf("%d",&np);
	fflush(stdin);

	//IMPRIMIR NUMERO DE PERIODOS
	printf("np: %d\n",np);

	for(a=0;a<np;a++)
	{
		for ( d = 0; d< n; d++)
			{
				aux[d]=0;
			}
		for(b=0;b<n;b++)
		{

			for(c=0;c<n;c++)
			{
				aux[b] += v[c]*m[c][b];
				printf("aux[%d]=%f * %f\n",b,v[b],m[b][c] );
			}
			
			//printf("%f\t",aux[b]);
		}
		printf("\n");
		for (d=0;d<n;d++)
				v[d]=aux[d];

		for (d=0;d<n;d++)
				printf("%f\t",v[d]);
		printf("\n\n");
	}

	//IMPRIMIR VECTOR PI
	printf("\n\nVector Pi: ");
	for(a=0;a<n;a++)
		printf("%f\t",aux[a]);
}
